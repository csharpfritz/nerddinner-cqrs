﻿using System;

namespace NerdDinner.Messages.Events
{
    public class DinnerRescheduled : IEvent
    {

        public DateTime NewEventTime { get; set; }

        public Guid CorrelationId { get; set; }
        public DateTime ActingDateTimeUtc { get; set; }
        public string ActingLogonId { get; set; }
        public Guid AggregateId { get; set; }
        public int Version { get; set; }

        public override string ToString()
        {
            return string.Format("Rescheduled to: {0}", NewEventTime);
        }

    }
}