﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NerdDinner.Messages.Events
{
    public class DinnerCreated : IEvent
    {

        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime EventDate { get; set; }
        public string HostedBy { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }
        public string ContactPhone { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public Guid CorrelationId { get; set; }
        public DateTime ActingDateTimeUtc { get; set; }
        public string ActingLogonId { get; set; }

        public Guid AggregateId { get; set; }

        public int Version { get; set; }

        public override string ToString()
        {
            return string.Format("Dinner created for '{0}' with title: {1}", EventDate, Title);
        }
    }
}
