﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NerdDinner.Messages.Commands
{

    public class CreateDinner : ICommand
    {
        public CreateDinner()
        {
            CorrelationId = Guid.NewGuid();
        }

        public Guid CorrelationId { get; set; }
        public DateTime ActingDateTimeUtc { get; set; }
        public string ActingLogonId { get; set; }
        public Guid AggregateId { get; set; }
        public int Version { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime EventDate { get; set; }

        public string HostedByLogon { get; set; }

        public string Address { get; set; }

        public virtual string Country { get; set; }

        public string ContactPhone { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

    }
}
