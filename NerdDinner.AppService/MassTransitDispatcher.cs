﻿using EventStore;
using EventStore.Dispatcher;

using Magnum.Reflection;

namespace NerdDinner.AppService
{
    public class MassTransitDispatcher : IDispatchCommits {
        public void Dispose()
        {
            // Nothing to do here
        }

        public void Dispatch(Commit commit)
        {

            foreach (var evt in commit.Events)
            {
                this.FastInvoke(new[] {evt.Body.GetType()}, "Publish", evt.Body);
            }

        }

        protected void Publish<T>(T msg) where T : class
        {
            AppService._Bus.Publish(msg, ctx => {});
        }
    }
}