using System;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using MassTransit;

using NerdDinner.Helpers;
using NerdDinner.Messages;
using NerdDinner.Messages.Commands;
using NerdDinner.Models;
using NerdDinner.ReadModel;

namespace NerdDinner.Controllers {

    [HandleErrorWithELMAH]
    public class DinnersController : Controller
    {

        #region Stuff We're Not Interested in...

        IDinnerRepository dinnerRepository;

        //
        // Dependency Injection enabled constructors

        public DinnersController()
            : this(new DinnerRepository()) {
        }

        public DinnersController(IDinnerRepository repository) {
            dinnerRepository = repository;
        }

        //
        // GET: /Dinners/
        //      /Dinners/Page/2
        //      /Dinners?q=term

        public ActionResult Index(string q, int? page) {

            const int pageSize = 25;

            IQueryable<Dinner> dinners = null;

            //Searching?
            if (!string.IsNullOrWhiteSpace(q))
                dinners = dinnerRepository.FindDinnersByText(q).OrderBy(d => d.EventDate);
            else 
                dinners = dinnerRepository.FindUpcomingDinners();

            var paginatedDinners = new PaginatedList<Dinner>(dinners, page ?? 0, pageSize);

            return View(paginatedDinners);
        }

        //
        // GET: /Dinners/Details/5

        public ActionResult Details(string id) {
            if (id == null) {
                return new FileNotFoundResult { Message = "No Dinner found due to invalid dinner id" };
            }

            var repo = new DinnerListRepository();
            var dinner = repo.GetById<DinnerListItem>(Guid.Parse(id));

            if (dinner == null) {
                return new FileNotFoundResult { Message = "No Dinner found for that id" };
            }

            var d = new Dinner()
                        {
                            Title = dinner.Title,
                            Description = dinner.Description,
                            Latitude = dinner.Latitude,
                            Longitude = dinner.Longitude,
                            EventDate = dinner.EventDate,
                            DinnerGuid = dinner.Id
                        };

            return View(d);
        }

        //
        // GET: /Dinners/Edit/5

        [Authorize]
        public ActionResult Edit(int id) {

            Dinner dinner = dinnerRepository.GetDinner(id);

            if (!dinner.IsHostedBy(User.Identity.Name))
                return View("InvalidOwner");

            return View(dinner);
        }

        //
        // POST: /Dinners/Edit/5

        [HttpPost, Authorize]
        public ActionResult Edit(int id, FormCollection collection) {

            Dinner dinner = dinnerRepository.GetDinner(id);

            if (!dinner.IsHostedBy(User.Identity.Name))
                return View("InvalidOwner");

            try {
                UpdateModel(dinner, "Dinner");

                dinnerRepository.Save();

                return RedirectToAction("Details", new { id=dinner.DinnerID });
            }
            catch {
                return View(dinner);
            }
        }

        #endregion

        #region Reschedule

        public ActionResult Reschedule(string id)
        {

            var repo = new DinnerListRepository();
            var d = repo.GetById<DinnerListItem>(Guid.Parse(id));

            return View(d);
        }

        [HttpPost]
        public ActionResult Reschedule(string dinnerId, DateTime newTime)
        {
            var cmd = new RescheduleDinner()
                          {
                              ActingDateTimeUtc = DateTime.UtcNow,
                              ActingLogonId = User.Identity.Name,
                              AggregateId = Guid.Parse(dinnerId),
                              CorrelationId = Guid.NewGuid(),
                              NewEventTime = newTime,
                              Version = 2
                          };

            Bus.Instance.PublishRequest(cmd, c =>
            {
                c.SetTimeout(TimeSpan.FromSeconds(10));
                c.Handle<BusResponse>(r => Debug.WriteLine(r.Message));
            });

            return RedirectToAction("Details", new { id = dinnerId });

        }

        //
        // GET: /Dinners/Create

        #endregion

        #region Create

        [Authorize]
        public ActionResult Create() {

            Dinner dinner = new Dinner() {
               EventDate = DateTime.Now.AddDays(7)
            };

            return View(new DinnerFormViewModel(dinner));
        } 

        //
        // POST: /Dinners/Create

        [HttpPost, Authorize]
        public ActionResult Create(Dinner dinner) {

            if (ModelState.IsValid) {
                NerdIdentity nerd = (NerdIdentity)User.Identity;
                dinner.HostedById = nerd.Name;
                dinner.HostedBy = nerd.FriendlyName;

                RSVP rsvp = new RSVP();
                rsvp.AttendeeNameId = nerd.Name;
                rsvp.AttendeeName = nerd.FriendlyName;
                dinner.RSVPs.Add(rsvp);

                //dinnerRepository.Add(dinner);
                //dinnerRepository.Save();

                var cmd = new CreateDinner()
                              {
                                  ActingDateTimeUtc = DateTime.UtcNow,
                                  ActingLogonId = nerd.Name,
                                  Address = dinner.Address,
                                  AggregateId = Guid.NewGuid(),
                                  ContactPhone = dinner.ContactPhone,
                                  CorrelationId = Guid.NewGuid(),
                                  Description = dinner.Description,
                                  Country = dinner.Country,
                                  EventDate = dinner.EventDate,
                                  HostedByLogon = nerd.Name,
                                  Title = dinner.Title,
                                  Latitude = dinner.Latitude,
                                  Longitude = dinner.Longitude
                              };
                Bus.Instance.PublishRequest(cmd, c =>
                                                     {
                                                         c.SetTimeout(TimeSpan.FromSeconds(10));
                                                         c.Handle<BusResponse>(r => Debug.WriteLine(r.Message));
                                                     });

                return RedirectToAction("Details", new { id=dinner.DinnerID });
            }

            return View(new DinnerFormViewModel(dinner));
        }

        #endregion

        #region More Nonsense

        //
        // HTTP GET: /Dinners/Delete/1

        [Authorize]
        public ActionResult Delete(int id) {

            Dinner dinner = dinnerRepository.GetDinner(id);

            if (dinner == null)
                return View("NotFound");

            if (!dinner.IsHostedBy(User.Identity.Name))
                return View("InvalidOwner");

            return View(dinner);
        }

        // 
        // HTTP POST: /Dinners/Delete/1

        [HttpPost, Authorize]
        public ActionResult Delete(int id, string confirmButton) {

            Dinner dinner = dinnerRepository.GetDinner(id);

            if (dinner == null)
                return View("NotFound");

            if (!dinner.IsHostedBy(User.Identity.Name))
                return View("InvalidOwner");

            dinnerRepository.Delete(dinner);
            dinnerRepository.Save();

            return View("Deleted");
        }

  
        protected override void HandleUnknownAction(string actionName)
        {
            throw new HttpException(404, "Action not found");
        }

        public ActionResult Confused()
        {
            return View();
        }

        public ActionResult Trouble()
        {
            return View("Error");
        }

        [Authorize]
        public ActionResult My()
        {

            NerdIdentity nerd = (NerdIdentity)User.Identity;
            var userDinners = from dinner in dinnerRepository.FindAllDinners()
                              where
                                (
                                String.Equals((dinner.HostedById ?? dinner.HostedBy), nerd.Name)
                                    ||
                                dinner.RSVPs.Any(r => r.AttendeeNameId == nerd.Name || (r.AttendeeNameId == null && r.AttendeeName == nerd.Name)) 
                                )
                              orderby dinner.EventDate
                              select dinner;

            return View(userDinners);
        }

        public ActionResult WebSlicePopular()
        {
            ViewData["Title"] = "Popular Nerd Dinners";
            var model = from dinner in dinnerRepository.FindUpcomingDinners()
                                        orderby dinner.RSVPs.Count descending
                                        select dinner;
            return View("WebSlice",model.Take(5));
        }

        public ActionResult WebSliceUpcoming()
        {
            ViewData["Title"] = "Upcoming Nerd Dinners";
            DateTime d = DateTime.Now.AddMonths(2);
            var model = from dinner in dinnerRepository.FindUpcomingDinners()
                        where dinner.EventDate < d
                        orderby dinner.EventDate descending
                    select dinner;
            return View("WebSlice", model.Take(5));
        }

        #endregion

    }
}