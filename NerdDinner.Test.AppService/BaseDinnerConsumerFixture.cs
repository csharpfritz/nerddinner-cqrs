﻿using System;

using CommonDomain.Persistence;

using MassTransit;

using Moq;

using NerdDinner.Messages;

using NUnit.Framework;

namespace NerdDinner.Test.AppService
{
    public class BaseDinnerConsumerFixture {

        protected Mock<IRepository> _MockRepo;
        protected Mock<IConsumeContext> _MockContext;

        protected static Action<ISendContext<BusResponse>> NullAction
        {
            get { return It.IsAny<Action<ISendContext<BusResponse>>>(); }
        }

        [SetUp]
        public void SetupTest()
        {
            _MockRepo = new Mock<IRepository>();
            _MockContext = new Mock<IConsumeContext>();
        }
    }
}